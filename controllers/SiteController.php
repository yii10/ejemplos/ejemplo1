<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionMensaje(){
        return $this->render('vista1');
    }
    
    public function actionPresentar(){
        return $this->render('presentar', [
            "mensaje" => "Probando argumentos"
        ]);
    }
    
    public function actionMensaje3(){
        $salida = "<ul>";
        for($i=1;$i<=100;$i+=2){
          $salida.="<li>$i</li>";  
        }
        $salida.="</ul>";
        return $this->render('listado',[
            "datos" => $salida
        ]);
    }
    
    public function actionMensaje4(){
        $salida = "<ul>";
        for($i=2;$i<=100;$i+=2){
          $salida.="<li>$i</li>";  
        }
        $salida.="</ul>";
        return $this->render('listado',[
            "datos" => $salida
        ]);
    }
    
    public function actionFotos($id){
        $fotos=[
          'OIP.jpg',
          'musica.jpg'
        ];
        return $this->render('fotos', [
            "foto" => $fotos[$id-1]
        ]);
    }

}

